package com.smith9.library.bean

import org.apache.commons.beanutils.ConvertUtils
import org.apache.commons.beanutils.BeanUtils
import org.joda.time.DateTime
import org.apache.commons.beanutils.converters.DateConverter
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/**
 * Created: Mar 16, 2011
 * @author david
 */
class MapToBean {
    public void setJodaDateTimeFormat(String format) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format)
        DateTimeConverter dateConverter = new DateTimeConverter(formatter)
        ConvertUtils.register(dateConverter, DateTime.class);
    }

    public void setDateFormat(String format) {
        DateConverter dateConverter = new DateConverter();
        dateConverter.setPattern(format);
        ConvertUtils.register(dateConverter, Date.class);
    }

    public Object convert(Class beanType, Map properties) {
        Map filtered = properties.findAll { it.value != null}
        Object bean = beanType.newInstance()
        BeanUtils.populate(bean, filtered)
        return bean
    }
}
