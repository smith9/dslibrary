package com.smith9.library.bean

import org.apache.commons.beanutils.Converter
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime

/**
 * Created: Mar 16, 2011
 * @author david
 */
class DateTimeConverter implements Converter {
    DateTimeFormatter formatter

    public DateTimeConverter() {
        formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    }

    public DateTimeConverter(DateTimeFormatter formatter) {
        this.formatter = formatter
    }

    public Object convert(Class targetType, Object input) {
        DateTime time = formatter.parseDateTime((String)input)
        return time;
    }
}
