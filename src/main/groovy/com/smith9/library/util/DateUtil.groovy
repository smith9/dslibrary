package com.smith9.library.util

import org.joda.time.DateTime

/**
 * Created: 28/03/11
 * @author david
 */
class DateUtil {
    public static String FORMAT_WITH_SECONDS = "yyyy-MM-dd_HH_mm_ss"
    /**
     * Create a string for the date that can be used in a filename
     * @param date
     * @return
     */

    public static String asFilenameString(DateTime date) {
        return date.toString(FORMAT_WITH_SECONDS)
    }
}
