package com.smith9.library.token

import groovy.text.SimpleTemplateEngine
import groovy.text.Template

class DefaultTokenReplace implements TokenReplace {
    public String replaceAll(String input, Map replacements) {
        SimpleTemplateEngine engine = new SimpleTemplateEngine()
        Template template = engine.createTemplate(input)
        Writable writable = template.make(replacements)
        return writable.toString()
    }
}
