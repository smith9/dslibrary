package com.smith9.library.csv

/**
 * Created: Mar 16, 2011
 * @author david
 */
public class CsvUtil {
    public static Map<String, Integer> calcIndices(List<String> headerRow) {
        final Map<String, Integer> indices = [:]
        headerRow.eachWithIndex { String column, int index ->
            indices[column] = index
        }
        return indices
    }

    public static LinkedHashMap<String, String> toMap(List<String> keys, List<String> values) {
        if(values.size() != keys.size()) {
            throw new IllegalArgumentException("Size of keys and values must match: $keys, $values")
        }
        LinkedHashMap<String, String> map = new LinkedHashMap()
        keys.eachWithIndex { String key, Integer index ->
            map[key] = values[index]
        }
        return map
    }
}
