package com.smith9.library.csv.opencsv

import au.com.bytecode.opencsv.CSVReader
import com.smith9.library.csv.CsvReader
import com.smith9.library.csv.CsvUtil

/**
 * Created: Mar 15, 2011
 * @author david
 */
class OpenCsvReader implements CsvReader {
    CSVReader reader
    List<String> header
    List<String> currentLine
    Map<String, Integer> columnIndices

    public OpenCsvReader(Reader read) {
        reader = new CSVReader(read)
        header = reader.readNext() as List<String>
        calcIndices()
    }

    public OpenCsvReader(File csvFile) {
        this(new FileReader(csvFile))
    }

    private void calcIndices() {
        columnIndices = CsvUtil.calcIndices(header)
    }

    public List<String> getHeader() {
        return header
    }

    public List<String> readNext() {
        currentLine = reader.readNext() as List<String>
        return currentLine
    }

    public LinkedHashMap<String, String> readNextAsMap() {
        readNext()
        if (currentLine != null) {
            return CsvUtil.toMap(header, currentLine)
        }
        return null;
    }

    public String getCurrentValue(String column) {
        int index = columnIndices[column]
        if (index == null) {
            throw new IllegalArgumentException("Column $column was not present in the header or the header was not present")
        }
        return currentLine[index]
    }
}
