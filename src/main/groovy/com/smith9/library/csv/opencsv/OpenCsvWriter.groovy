package com.smith9.library.csv.opencsv

import com.smith9.library.csv.CsvWriter
import au.com.bytecode.opencsv.CSVWriter

/**
 * Created: 24/03/11
 * @author david
 */
class OpenCsvWriter implements CsvWriter{
    private CSVWriter writer

    public OpenCsvWriter(Writer writer) {
        this.writer = new CSVWriter(new BufferedWriter(writer))
    }

    public OpenCsvWriter(File csvFile) {
        this(new FileWriter(csvFile))
    }

    void writeRow(List<String> row) {
        writer.writeNext(row as String[])
    }

    void close() {
        writer.close()
    }
}
