package com.smith9.library.csv

/**
 * Created: Mar 15, 2011
 * @author david
 */
public interface CsvReader {
    public List<String> getHeader()
    public List<String> readNext()
    public String getCurrentValue(String column)
    public LinkedHashMap<String, String> readNextAsMap()
}