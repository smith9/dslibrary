package com.smith9.library.sql

import groovy.sql.Sql
import com.smith9.library.csv.CsvWriter
import com.smith9.library.csv.opencsv.OpenCsvWriter
import groovy.sql.GroovyRowResult
import groovy.sql.GroovyResultSetExtension
import groovy.sql.GroovyResultSet
import com.smith9.library.token.DefaultTokenReplace

/**
 * Created: 25/03/11
 * @author david
 */
class DefaultSqlToCsv implements SqlToCsv {

    public void toCsv(File csv, Sql runner, String query, Map<String, String> replacements) {
        String replacedQuery = new DefaultTokenReplace().replaceAll(query, replacements)
        if(!csv.getParentFile().exists()) {
            csv.getParentFile().mkdirs()
        }

        CsvWriter writer = new OpenCsvWriter(csv)
        boolean firstRow = true
        runner.eachRow(replacedQuery) { row ->
            def rowResult = row.toRowResult()
            if(firstRow) {
                firstRow = false
                writer.writeRow(rowResult.keySet().collect { it } as List<String>)
            }
            writer.writeRow(rowResult.values() as List<String>)
        }
        writer.close()
    }
}
