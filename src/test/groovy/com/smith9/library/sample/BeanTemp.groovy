package com.smith9.library.sample

import org.apache.commons.beanutils.BeanUtils
import org.apache.commons.beanutils.ConvertUtils

/**
 * Created: Mar 16, 2011
 * @author david
 */
class BeanTemp {
    String name
    int age

    public static void main(String[] args) {
        LinkedHashMap map = [name: "david", age: 21]
        BeanTemp bean = new BeanTemp()
        BeanUtils.populate(bean, map)
        ConvertUtils
        System.out.println("bean = " + bean);
    }

    public String toString ( ) {
    return "Bean{" +
    "name='" + name + '\'' +
    ", age=" + age +
    '}' ;
    }}
