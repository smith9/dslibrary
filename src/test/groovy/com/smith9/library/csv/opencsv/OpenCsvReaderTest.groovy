package com.smith9.library.csv.opencsv

import org.junit.Test
import static junit.framework.Assert.assertEquals
import static junit.framework.Assert.assertNull

/**
 * Created: Mar 15, 2011
 * @author david
 */
class OpenCsvReaderTest {
    @Test
    void testReaderConstructor() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        Reader fileReader = new FileReader(input)
        OpenCsvReader reader = new OpenCsvReader(fileReader)
        reader.readNext()
        List expected = ["a1", "a2 2", "a3", "a,4", "a5"]
        List actual = reader.getHeader().collect { reader.getCurrentValue(it)}
        assertEquals(expected, actual)
    }

    @Test
    void testHeader() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        List<String> header = reader.getHeader()
        junit.framework.Assert.assertEquals(["h1", "h2", "h,3 ", "h4", "h5"], header)
    }
    
    @Test
    void testGetCurrentValue() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        reader.readNext()
        List expected = ["a1", "a2 2", "a3", "a,4", "a5"]
        List actual = reader.getHeader().collect { reader.getCurrentValue(it)}
        assertEquals(expected, actual)
    }

    @Test (expected=IllegalArgumentException.class)
    void testGetCurrentValueBadColumn() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        reader.readNext()
        reader.getCurrentValue("not a column in the header")
    }

    @Test
    void testRowsAsMap() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        ["h1", "h2", "h,3 ", "h4", "h5"]
        assertEquals([h1:"a1",h2:"a2 2","h,3 ":"a3",h4:"a,4",h5:"a5"], reader.readNextAsMap())
        assertEquals([h1:"b1",h2:"b2","h,3 ":"b3",h4:"b,4",h5:"b5"], reader.readNextAsMap())
        2.times {
            assertNull(reader.readNext())
        }

    }

    @Test
    void testEmptyAsMap() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputEmpty.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertNull(reader.getHeader())
        2.times {
            assertNull(reader.readNextAsMap())
        }
    }
    @Test
    void testOnlyHeaderAsMap() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputOnlyHeader.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertEquals(["h1", "h2", "h,3 ", "h4", "h5"], reader.getHeader())
        2.times {
            assertNull(reader.readNextAsMap())
        }
    }

    @Test
    void testRows() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/input.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertEquals(["a1","a2 2","a3","a,4","a5"], reader.readNext())
        assertEquals(["b1","b2","b3","b,4","b5"], reader.readNext())
        2.times {
            assertNull(reader.readNext())
        }

    }
    @Test
    void testEmpty() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputEmpty.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertNull(reader.getHeader())
        2.times {
            assertNull(reader.readNext())
        }
    }
    @Test
    void testOnlyHeader() {
        File input = new File(getClass().getClassLoader().getResource('csv/input/inputOnlyHeader.csv').toURI())
        OpenCsvReader reader = new OpenCsvReader(input)
        assertEquals(["h1", "h2", "h,3 ", "h4", "h5"], reader.getHeader())
        2.times {
            assertNull(reader.readNext())
        }
    }

}
