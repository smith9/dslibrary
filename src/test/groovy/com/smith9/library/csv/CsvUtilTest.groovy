package com.smith9.library.csv

import org.junit.Test
import static junit.framework.Assert.assertEquals

/**
 * Created: Mar 16, 2011
 * @author david
 */
class CsvUtilTest {
    @Test
    public void testCalcIndices() {
        assertEquals([c0:0, c1:1, c2:2], CsvUtil.calcIndices(["c0", "c1", "c2"]))
    }
    @Test
    public void testCalcIndicesNoColumns() {
        assertEquals([:], CsvUtil.calcIndices([]))
    }
    @Test
    public void testCalcIndicesNullColumns() {
        assertEquals([:], CsvUtil.calcIndices(null))
    }
    @Test
    public void testToMap() {
        assertEquals([k0:"v0", k1:"v1", k2:"v2"], CsvUtil.toMap(["k0", "k1", "k2"], ["v0", "v1", "v2"]))
    }
    @Test (expected=IllegalArgumentException.class)
    public void testToMapEmptyValues() {
        CsvUtil.toMap(["k0", "k1", "k2"], [])
    }
    @Test
    public void testToMapNullValues() {
        assertEquals([k0:"v0", k1:null, k2:"v2"], CsvUtil.toMap(["k0", "k1", "k2"], ["v0", null, "v2"]))
    }
}
