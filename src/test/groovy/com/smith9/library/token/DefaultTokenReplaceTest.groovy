package com.smith9.library.token

import org.junit.Test
import static junit.framework.Assert.assertEquals

class DefaultTokenReplaceTest {
    String INPUT =
'''
this is some text that contains some ${TOKEN} ${TOKEN}
it spans multiple lines
can have ${TOKEN} ${name} multiple tokens on each line
and can contain the token char on its own \\$
'''
    String EXPECTED =
'''
this is some text that contains some hello hello
it spans multiple lines
can have hello david multiple tokens on each line
and can contain the token char on its own $
'''
    @Test
    public void testReplaceTokens() {
        String result = new DefaultTokenReplace().replaceAll(INPUT, [TOKEN:"hello", name:"david"])
        assertEquals(EXPECTED, result)
    }
}
