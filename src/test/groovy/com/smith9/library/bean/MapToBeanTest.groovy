package com.smith9.library.bean

import org.junit.Test
import static junit.framework.Assert.assertEquals
import org.joda.time.format.DateTimeFormatter
import java.text.SimpleDateFormat
import org.joda.time.DateTime
import static junit.framework.Assert.assertNull

/**
 * Created: Mar 16, 2011
 * @author david
 */
class MapToBeanTest {
    @Test
    public void testConvert() {
        Map properties = [
            dateTimeValue: "2011-03-16 20:42:55",
            integerValue: "21",
            dateValue: "18/04/2011",
            intValue: "18",
            stringValue: "some string",
            doubleValue: "20.55555555555555"]

        MapToBean converter = new MapToBean()
        converter.setJodaDateTimeFormat ("yyyy-MM-dd HH:mm:ss")
        converter.setDateFormat ("dd/MM/yyyy")
        converter
        Bean bean = converter.convert(Bean.class, properties)
        assertEquals(20.55555555555555d, bean.doubleValue)
        assertEquals(21, bean.integerValue)
        assertEquals(18, bean.intValue)
        assertEquals("some string", bean.stringValue)
        assertEquals("2011-03-16 20:42:55", bean.dateTimeValue.toString("yyyy-MM-dd HH:mm:ss"))
        assertEquals("18/04/2011", toString(bean.dateValue))
    }
    @Test
    public void testConvertLessPropertiesThanBean() {
        Map properties = [
            dateTimeValue: "2011-03-16 20:42:55",
            stringValue: "some string",
            integerValue:null]

        MapToBean converter = new MapToBean()
        converter.setJodaDateTimeFormat ("yyyy-MM-dd HH:mm:ss")
        converter.setDateFormat ("dd/MM/yyyy")
        Bean bean = converter.convert(Bean.class, properties)
        assertEquals(0d, bean.doubleValue)
        assertNull(bean.integerValue)
        assertEquals(0, bean.intValue)
        assertEquals("some string", bean.stringValue)
        assertEquals("2011-03-16 20:42:55", bean.dateTimeValue.toString("yyyy-MM-dd HH:mm:ss"))
        assertNull(bean.dateValue)
    }

    private static String toString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy")
        return format.format(date)
    }

}
