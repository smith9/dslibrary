package com.smith9.library.bean

import org.joda.time.DateTime

/**
 * Created: Mar 16, 2011
 * @author david
 */
class Bean {
    DateTime dateTimeValue
    Integer integerValue
    Date dateValue
    int intValue
    String stringValue
    double doubleValue
}
