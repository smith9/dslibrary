package com.smith9.library.util

import org.junit.Test
import static junit.framework.Assert.assertEquals
import org.joda.time.DateTime

/**
 * Created: 28/03/11
 * @author david
 */
class DateUtilTest {
    @Test
    public void testasFilenameString() {
        final DateTime dateTime = new DateTime(2011, 03, 28, 17, 20, 0, 0)
        assertEquals("2011-03-28_17_20_00", DateUtil.asFilenameString(dateTime))
    }
}
