package com.smith9.library.sql

import org.junit.Test
import groovy.sql.Sql
import static junit.framework.Assert.assertEquals

/**
 * Created: 25/03/11
 * @author david
 */
class DefaultSqlToCsvTest {

    @Test
    public void testToCsvNoReplacements() {
        File outputDir = new File("target/DefaultSqlToCsvTest")
        outputDir.mkdirs()
        File csv = new File(outputDir, "sql.csv")
        SqlToCsv runner = new DefaultSqlToCsv()

        Sql sql = createTestConnection()
        createTestTable(sql)
        final String queryString = "select * from test;"
        final LinkedHashMap replacements = [:]
        runner.toCsv(csv, sql, queryString, replacements)
        assertEquals(""""ID","FORENAME","SURNAME","DESCRIPTION"
"1","David","Smith",
"2","Joe","Blogs","code monkey"
"3","Albert","Einstein","Scientist"
""", csv.text)
    }

    @Test
    public void testToCsvWithReplacements() {
        File outputDir = new File("target/DefaultSqlToCsvTest")
        outputDir.mkdirs()
        File csv = new File(outputDir, "sql.csv")
        SqlToCsv runner = new DefaultSqlToCsv()

        Sql sql = createTestConnection()
        createTestTable(sql)
        final String queryString = 'select ${COLUMNS} from test;'
        final LinkedHashMap replacements = [COLUMNS:"id,forename,surname"]
        runner.toCsv(csv, sql, queryString, replacements)
        assertEquals(""""ID","FORENAME","SURNAME"
"1","David","Smith"
"2","Joe","Blogs"
"3","Albert","Einstein"
""", csv.text)
    }

    def createTestTable(Sql sql) {
        File testData = new File(getClass().getClassLoader().getResource('sql/input/database.csv').toURI())
        createTableFromCsv(sql, "test", testData)
    }

    private Sql createTestConnection() {
        String sqlusername = "sa"
        String sqlpassword = ""
        String driver = "org.h2.Driver"
        String connectionstring = "jdbc:h2:temp"

        Sql sql = Sql.newInstance(connectionstring, sqlusername, sqlpassword, driver)
        return sql
    }

    public void createTableFromCsv(Sql sql, String tableName, File csv) {
        // FIX - when run on a new machine, need to avoid doing drop when table does not exist
        sql.execute("drop table ${Sql.expand tableName};")
        sql.execute("create table ${Sql.expand tableName} as select * from CSVREAD('${Sql.expand csv.absolutePath}');")
    }

//    public void createDataBase(List<String> header) {
//        String sqlusername = "sa"
//        String sqlpassword = ""
//        String driver = "org.h2.Driver"
//        String connectionstring = "jdbc:h2:~/temp"
//
//        Sql sql = Sql.newInstance(connectionstring, sqlusername, sqlpassword, driver)
//        sql.execute("create table sqltest(id int primary key, firstname varchar, lastname varchar);")
//        sql.eachRow("show tables;") {
//            println it
//        }
//    }
//    private void xxx() {
//        ResultSet rs = Csv.getInstance().
//            read("data/test.csv", null, null);
//        ResultSetMetaData meta = rs.getMetaData();
//        while (rs.next()) {
//            for (int i = 0; i < meta.getColumnCount(); i++) {
//                System.out.println(
//                    meta.getColumnLabel(i + 1) + ": " +
//                    rs.getString(i + 1));
//            }
//            System.out.println();
//        }
//        rs.close();
//    }
}
